---
title: "Does Sudetenland exist? The border is distinguishable even 70 years after the expulsion of Germans"
perex: "Statistics concerning unemployment, education quality, and voter turnouts in the Czech Republic are still showing the border of the historical area of Sudetenland. The wave of new residents is also reflected in the largest minority in the border area today – Slovaks. Take a look at the detailed maps."
description: "Statistics concerning unemployment, education quality, and voter turnouts in the Czech Republic are still showing the border of the historical area of Sudetenland. The wave of new residents is also reflected in the largest minority in the border area today – Slovaks. Take a look at the detailed maps."
authors: ["Jan Boček", "Jan Cibulka"]
published: "10 June 2016"
coverimg: https://samizdat.cz/data/sudety-clanek/www/media/vysidleni.jpg
# coverimg_note: "Foto <a href='#'>ČTK</a>"
# socialimg: https://interaktivni.rozhlas.cz/brexit/media/cover.jpg
url: "sudetenland"
# styles: []
# libraries: [d3, topojson, jquery, highcharts, leaflet, inline-audio]
---

The government approved expulsion of Germans from the Sudetenland to the Soviet zone started exactly seventy years ago. Three million of the old German residents were quickly replaced by new settlers: Czechs coming from the inland, Slovaks, Hungarians, Czech emigrants coming back from exile, but also Greeks and Macedonians.

And until today, the border area stays different from the rest of the country. Similarly to Germany, where the statistics still distinctly show [the division to the eastern and western parts](https://www.washingtonpost.com/news/worldviews/wp/2014/10/31/the-berlin-wall-fell-25-years-ago-but-germany-is-still-divided/) and [the life expectancy still reflects the Iron Curtain](http://data.blog.ihned.cz/c1-62250230-data-na-vychode-byvale-zelezne-opony-se-umira-o-dva-az-sest-let-driv), the former Sudetenland is also distinguishable on maps of the Czech Republic.

At the same time, each region of the former Sudetenland has its own specific problems. Northern Bohemia, often called “The Wealthy Sudetenland” for its rich pre-war industry, has different issues to deal with than the large areas of Jeseníky, or Šumava. Have a look at what difficulties each part of the border area faces.

<aside class="small">
  <h3>The expulsion of Germans as reported in the Czechoslovakian and Czech Radio:</h3>
  <ul>
    <li>18 July 1946: <a href="https://samizdat.blob.core.windows.net/zvuky/1946-benes.mp3" target="_blank">Speech of president Edvard Beneš in Hrabyně</a></li>
    <li>29 October 1946: <a href="https://samizdat.blob.core.windows.net/zvuky/1946-nosek-karlovy-vary.mp3" target="_blank">Václav Nosek, Minister of the Interior, comments on the last transport of Sudeten Germans leaving from Karlovy Vary</a></li>
    <li>5 November 1946: <a href="https://samizdat.blob.core.windows.net/zvuky/1946-nosek-projev.mp3" target="_blank">The Minister of the Interior Václav Nosek’s speech on expulsion of Germans</a></li>
    <li>30 December 1969: <a href="https://samizdat.blob.core.windows.net/zvuky/1969-nemecka-otazka.mp3" target="_blank">The German Question in Czechoslovakia (Current Missions of the Czechoslovak Communist Party’s National Policy)</a></li>
    <li>12 July 1979: <a href="https://samizdat.blob.core.windows.net/zvuky/1979-kamapan.mp3" target="_blank">On the campaign triggered in the FRG – the expulsion of Germans from the border area</a></li>
    <li>29 March 1993: <a href="https://samizdat.blob.core.windows.net/zvuky/1993-havel-ozveny.mp3" target="_blank">Václav Havel – It is morally wrong to approve of the expulsion of Germans (Echoes of the Day)</a></li>
    <li>10 October 1993: <a href="https://samizdat.blob.core.windows.net/zvuky/1993-havel.mp3" target="_blank">Václav Havel on the issue of Sudeten Germans (Talks from Lány)</a></li>
    <li>1995: <a href="https://samizdat.blob.core.windows.net/zvuky/1995-klaus.mp3" target="_blank">Václav Klaus on the expulsion of Sudeten Germans</a></li>
    <li>31 May 2005: <a href="https://samizdat.blob.core.windows.net/zvuky/2005-postoloprty.mp3" target="_blank">Forgotten May in Postoloprty – a collage report on the perzekuce.cz project</a></li>
    <li>19 November 2010: <a href="https://samizdat.blob.core.windows.net/zvuky/2010-ozveny.mp3" target="_blank">Media debates on post-war expulsion of Germans are getting more intense (Echoes of the Day)</a></li>
    <li>June 2016: <a href="https://samizdat.blob.core.windows.net/zvuky/2016-doopravdy.mp3" target="_blank">What really happened – historian Oldřich Tůma</a></li>
  </ul>
</aside>

## Slovak trace is the most distinguishable

According to historical documents, there were 2.5 million people living in Sudetenland at the end of March 1947, about half a million less than before the war. Third of them were the original residents, a third were Czechoslovaks from the inland, and a third was a combination of national and ethnic minorities.

Soon enough, Slovak became the most common language after Czech. About 200 thousand Slovaks, including tens of thousands of Roma people from Eastern Slovakia, were in the first wave of newcomers,  and more were to come. Free land was tempting for Czech and Slovak emigrants from Poland, Vienna, Hungary, Romania, and Subcarpathian Ruthenia. But only a part of them had a clear idea of their destination.

“The Romanian Slovaks, who formed the biggest part of the wave of ‘re-emigrants’, only learned on the train that they were not going to Slovakia, to their family roots,” says [Matěj Spurný in his book Stories of the Sudetenland](http://www.antikomplex.cz/spolecnost-v-sudetech-po-roce-1945.html). “Dozens of thousands of these people – there were about 50 thousand of Slovaks in Romania, and it is not clear how many of them moved to Sudetenland – were taken to Jeseníky, to the remote parts of Southern Bohemia and Southern Moravia, and to the deserted areas of Western Bohemia. Most of them were given three hectares of land and were expected to become forest workers.”

About 300 thousand of ethnic Slovaks came to the area of former Sudetenland, and it is their mark that is the most distinguishable there today. During the census of the population in 2011, almost 40 thousand of local residents registered as Slovak nationals.

The Sudeten border on the map copies the area where there were more people speaking German than Czech in the 1930s (based on a [language map](http://nemci.euweb.cz/m2.gif) originally published in Ortslexikon Sudetenland in 1987). The map shows that Czech Slovaks inhabit the same area today as Czech Germans did in the past. However, there are much less of them; Sudeten Germans formed the majority in the area, whereas Slovak nationals form less than two percent of local population today.

<aside class="big">
  <figure>
    <img src="https://samizdat.cz/data/sudetenland/www/media/slovaci-en.jpg" width="1000" height="704">
  </figure>
  <figcaption>
    Source: <a href="https://vdb.czso.cz/vdbvo2/faces/cs/index.jsf?page=statistiky#katalog=30261" target="_blank">Census of the population, buildings and apartments in 2011</a>
  </figcaption>
</aside>

Despite the thorough expulsion, part of the Germans stayed in Sudetenland, and many of them were actually banned from leaving.  Post-war Czechoslovakia needed them because of their extensive knowledge of glass-making and textile industries. “In the late 60s, they could petition for relocation, and after paying several tens of thousands crowns, they could leave,” says Tomáš Lindner in the Respekt magazine, [telling the story of the German community in Abertamy](https://www.respekt.cz/tydenik/2013/14/posledni-nemci-v-sudetech). Part of them stayed, and their descendants now create the second largest minority in the Czech border area. Over 13 thousand of locals registered as German nationals.

<aside class="small">
  <figure>
    <iframe src="https://datawrapper.dwcdn.net/QvcwW/1/" width="300" height="325" scrolling="no" frameborder="0"></iframe>
  </figure>
</aside>

Other nations, whose representatives moved to the borderlands after the war, did not leave a distinct mark. In the past 70 years, tens of thousands of Hungarians have left, or do not acknowledge their origin. This was probably similar for the 20 thousand of Greek communists who came here after their loss in the Greek civil war, and who mostly settled in Northern Moravia in the early 1950s. Two-and-a-half thousand of them came to Bruntál, a town with a population of twenty thousand. Not many of their descendants registered as Greek during the last census, though.

## Northern Moravia remained deserted

Despite the difficult natural conditions, the border area was one of the most densely populated regions in pre-war Czechoslovakia. After 1945, only half a million Czechoslovaks and tens of thousands of Germans remained there, which is about a fifth of the original population.

Not all areas had the same priority during the post-war populating of Sudetenland: for the then government, it was crucial to first populate the industrial north of Bohemia. The Northern and Southern Moravian border areas were less important, although the plan was to fully replace the population there as well. But unlike in the north, the post-war planners did not intend to use these areas for industry, but for recreation.

Šumava, Český les, and Southern Bohemian border were supposed to be left deserted. For the communists, the border with West Germany was a potential battle field, where the first serious clashes would take place in the event of a global conflict.

The current statistics of the population density show that not everything went according to plan.

<aside class="big">
  <figure>
    <img src="https://samizdat.cz/data/sudetenland/www/media/hustota-en.jpg" width="1000" height="704">
  </figure>
  <figcaption>
    Source: <a href="https://vdb.czso.cz/vdbvo2/faces/cs/index.jsf?page=statistiky#katalog=30261" target="_blank">Census of the population, buildings and apartments in 2011</a>
  </figcaption>
</aside>

The first and biggest wave of new residents was headed to Northern Bohemia, populating of which was successful, albeit uneven. That was not necessarily wrong – the communist planners wanted to populate the industrial areas; the agriculture in the mountains was not prospective for them, so the villages could remain deserted. The plan that failed completely, however, was populating of Northern Moravia, which, along with  Šumava and Český les, is still the most sparsely populated area in the Czech Republic.

Despite the low population density, the west and south of the former Sudetenland now struggles with housing shortage. The somewhat paradoxical situation where the empty villages were replaced by the current lack of housing was partially caused by events in 1959. In that year, the communist government ordered to demolish all the abandoned buildings in the area; over 34,000 objects were then destroyed. Some of them can be found on the [map of depopulated villages and destroyed sights](https://www.google.com/maps/d/viewer?hl=en_US&mid=1Nv7dugZxH5hTG6eMpG39qz5rXoQ). The zanikleobce.cz web database, on which the map is based, lists more than seven hundred dilapidated or destroyed villages.

## Unemployment higher by a quarter

Until the end of 1930s, Sudetenland was one of the economically strongest regions in the country as well. “The expulsion of 2.6 million Czech Germans meant the loss of a million people of working age: they were hard-working farmers and small craftsmen, factory workers, but also educated intellectuals, who were part of the Czech culture. A third of arable land and more than three-quarters of meadows and pastures were located there, along with most of the vineyards, more than a third of the forests, and a large number of ponds and water areas,” says Matěj Spurný.

However, the statistics showing the economic failure of the border areas were kept secret during communism, which praised itself for full employment. The region’s underdevelopment fully manifested itself after 1989; the unemployment rate rose faster there than in any other parts of the country. The last census shows that in 2011, the unemployment rate in the former Sudetenland was by a quarter higher than in the inland (12.4% vs. 9.2%).

<aside class="big">
  <figure>
    <img src="https://samizdat.cz/data/sudetenland/www/media/nezamestnanost-en.jpg" width="1000" height="704">
  </figure>
  <figcaption>
    Source: <a href="https://www.czso.cz/" target="_blank">Czech Statistical Office</a>
  </figcaption>
</aside>

The lack of jobs is especially noticeable in the North Moravian part of Sudetenland, even though the situation in the rest of the borderlands is not much better. The largest proportion of families living below the level of poverty live in these areas, and, in the north of the country, social benefits paid by the state are above-average.

However, the notion that the rate of property or violent crime is higher in the border area has not been confirmed. Crime is still concentrated in big cities all over the country (the highest rate is in Prague). While the higher figures concerning crime only show in Northern Bohemia, the sparsely populated regions of Šumava, Český les, and Jeseníky are mostly crime-free.

## Elementary school is enough

The higher unemployment rate is connected to another trait of the region - low level of education. The proportion of people who only finished elementary school is the largest in the whole of the Czech Republic.

<aside class="big">
  <figure>
    <img src="https://samizdat.cz/data/sudetenland/www/media/vzdelani-en.jpg" width="1000" height="704">
  </figure>
  <figcaption>
    Source: <a href="https://vdb.czso.cz/vdbvo2/faces/cs/index.jsf?page=statistiky#katalog=30261" target="_blank">Census of the population, buildings and apartments in 2011</a>
  </figcaption>
</aside>

## Less people vote, and when they do, they vote against the system

The voter turnout for the parliamentary election in the border area is lower by 5 to 10 per cent compared to the Czech inland; and the protest parties, including the Communists, are more successful there.

<aside class="big">
  <figure>
    <img src="https://samizdat.cz/data/sudetenland/www/media/ucast-en.jpg" width="1000" height="704">
  </figure>
  <figcaption>
    Source: <a href="http://volby.cz/pls/ps2013/ps" target="_blank">Volby.cz</a>
  </figcaption>
</aside>

The popularity of the Communist Party became deeply rooted in the area right after it was populated. “The large number of citizens without tradition and steady property and other relations constituted great potential voters of the Communist Party, with which they shared the ethos of a new beginning and revolutionary change,” says Matěj Spurný of the Antikomplex association. “That was fully confirmed by the election in the spring of 1946 when the party got 53 percent of votes in the border area, compared to the average of 40 percent throughout the Czech lands.”

And the Communist Party is still successful today in many districts in Western Bohemia. One of the reasons is the fact that the proximity of the border with West Germany required the “most reliable” families and border guards to be moved there. However, as one can see on [the detailed map of the European elections in 2014](http://bl.ocks.org/michalskop/raw/df2c64ef89e9a5f9f41d/), each part of the borderland voted differently: ANO dominated in Northern Bohemia, whereas in Western Bohemia and Šumava it was TOP 09, partially thanks to tourists, and the clear winner in the Jeseníky area was ČSSD. The results in Znojmo area showed support for ANO, the Communists, and Christian Democrats.

Thus the area of former Sudetenland can hardly be considered a monolith. Although they are facing similar problems, each part of the former Sudetenland acts slightly differently – and not just during elections.